package be.jschoreels.demo.eventdriven.services.communicate.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Message.Builder.class)
public class Message {

    private String body;

    private String messageType;

    private Message(Builder builder) {
        body = builder.body;
        messageType = builder.messageType;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Message copy) {
        Builder builder = new Builder();
        builder.body = copy.getBody();
        builder.messageType = copy.getMessageType();
        return builder;
    }

    public String getBody() {
        return body;
    }

    public String getMessageType() {
        return messageType;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder()
    public static final class Builder {
        private String body;
        private String messageType;

        private Builder() {
        }

        public Builder withBody(String val) {
            body = val;
            return this;
        }

        public Builder withMessageType(String val) {
            messageType = val;
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }
}
