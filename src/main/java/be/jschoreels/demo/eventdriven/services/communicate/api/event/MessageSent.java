package be.jschoreels.demo.eventdriven.services.communicate.api.event;

import be.jschoreels.demo.eventdriven.services.communicate.api.domain.Message;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.ZonedDateTime;

@JsonDeserialize(builder = MessageSent.Builder.class)
public class MessageSent {

    private String user;

    private Message message;

    private ZonedDateTime sentTime;

    private MessageSent(Builder builder) {
        user = builder.user;
        message = builder.message;
        sentTime = builder.sentTime;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(MessageSent copy) {
        Builder builder = new Builder();
        builder.user = copy.getUser();
        builder.message = copy.getMessage();
        builder.sentTime = copy.getSentTime();
        return builder;
    }

    public String getUser() {
        return user;
    }

    public Message getMessage() {
        return message;
    }

    public ZonedDateTime getSentTime() {
        return sentTime;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder()
    public static final class Builder {
        private String user;
        private Message message;
        private ZonedDateTime sentTime;

        private Builder() {
        }

        public Builder withUser(String val) {
            user = val;
            return this;
        }

        public Builder withMessage(Message val) {
            message = val;
            return this;
        }

        public Builder withSentTime(ZonedDateTime val) {
            sentTime = val;
            return this;
        }

        public MessageSent build() {
            return new MessageSent(this);
        }
    }
}
